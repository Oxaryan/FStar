# FStar
My efforts to binging [FStar Language](http://fstar-lang.org) out of INRIA.

**This repo and all of it's sub-repos are under construction**

- [FStar Docs](https://github.com/0xaryan/FStarDocs) A clone of FStar docs in RTFD
- [FStarDocBootstrapper](https://github.com/0xaryan/FStarDocBootstrapper) Translates FStar official docs to reStructuredText
